SiteVibes.assembleWidgetScaffolding = function(){
    var ref = this;
    var activeCounter = 0;  //used to determine first tab, and maybe limit
                            //total number of tabs displayed in future

    var widgetHTML = 
        '<div id="sv_trending_widget_wrap" class="sv_trending_widget_init sv_trending_widget_'+this.trendingWidgetSide+'">\
            <div id="sv_trending_expando_wrap">\
                <div id="sv_trending_expando">What\'s Trending?</div>\
            </div>\
            <div id="sv_trending_widget_tab_wrap">\
                <ul></ul>\
            </div>\
            <div id="sv_trending_widget_content_wrap">\
                <div id="sv_more_content_left" class="sv_trending_arrow_left">\
                    <div class="arrow-left"></div>\
                </div>\
                <div id="sv_trending_widget_content">\
                    <div id="sv_trending_widget_content_inner"></div>\
                </div>\
                <div id="sv_more_content_right" class="sv_trending_arrow_right">\
                    <div class="arrow-right"></div>\
                </div>\
            </div>\
        </div>';
        
    var injectAfter = document.querySelector(this.trendingWidgetAfter);

    if(injectAfter != null){
        injectAfter.insertAdjacentHTML('afterend', widgetHTML);
        if(!this.trendingWidgetLoadOpen && !this.trendingWidgetInitBounce){
            var wWrap = document.getElementById('sv_trending_widget_wrap');
            wWrap.style[this.trendingWidgetSide]='-'+wWrap.offsetWidth+'px';
        }
    }
    
    //add listener for close button
    var close = document.getElementById('sv_trending_expando_wrap');
    close.addEventListener('click', function(e){
            ref.toggleWidgetDisplay();
    });
    
    //add event listener for load more action
    document.getElementById('sv_more_content_right')
    .addEventListener('click',function(e){

        if(this.className == 'sv_trending_arrow_right sv_arrow_disabled' ||
            ref.widgetPaginating != 2
        ){
            return;  //dont fire request if more arrow was disabled 
        }

        ref.svCounter++;
        ref.renderSeenWidgetItems('+');
        return;

    });

    //add event listener for load less action
    document.getElementById('sv_more_content_left')
    .addEventListener('click', function(e){

        if(this.className == 'sv_trending_arrow_left sv_arrow_disabled' ||
           ref.widgetPaginating != 2
        ){
            return;  //dont fire request if more arrow was disabled 
        }

        ref.svCounter--;
        ref.renderSeenWidgetItems('-');
        return;
    });
};

    //create the widgets html as a string
SiteVibes.createWidgetHtmlString = function(data){
    var itemsHTML = '';

    for(i=0;i<data.length;i++){

        var prices = data[i]._source.price;
        var was = decodeURIComponent(data[i]._source.price);
        var sale = '';
        var price_split = prices.split('|')
        if (price_split.length > 1) {
            was = price_split[0];
            sale = price_split[1];
        }
        
        itemsHTML += 
            '<div class="sv_trending_widget_content_item">\
                <div class="sv_thumb_wrap">\
                    <a href="'+data[i]._source.url+'"><img src="'+data[i]._source.image+'" alt="'+data[i]._source.name+'" /></a>\
                </div>\
                <div class="sv_desc_wrap">\
                    <span class="sv_name"><a href="'+data[i]._source.url+'" title="'+data[i]._source.name+'">'+data[i]._source.name+'</a></span>\
                    <span class="sv_price">'+was+'</span>\
                    <span class="sv_sale_price">'+sale+'</span>\
                </div>\
            </div>';

    }
    
    //if we have less than 3 inject some empty ones
    var toAdd = 3 - data.length;
    for(i=1;i<=toAdd;i++){
        itemsHTML += 
            '<div class="sv_trending_widget_content_item">\
                <div class="sv_thumb_wrap">\
                    <a href=""></a>\
                </div>\
                <div class="sv_desc_wrap">\
                    <span class="sv_name"><a href="" title=""></a></span>\
                    <span class="sv_price"></span>\
                    <span class="sv_sale_price"></span>\
                </div>\
            </div>';
    }

    return itemsHTML;
};